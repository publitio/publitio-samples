<?php
	header('Content-type: text/plain; charset=utf-8');
	require_once('publitio_api.php'); 
// Please update xxxx with your key and yyyy with your secret
	$publitio_api = new PublitioAPI('xxxx', 'xxxx');


    $currentDir = getcwd();
    $uploadDirectory = "/uploads/";

    $errors = []; // Store all possible errors here

    $fileExtensions = ['jpeg','jpg','png', 'webp']; // Show only this extensions and make sure we upload only them

    $fileName = $_FILES['myfile']['name'];
    $fileSize = $_FILES['myfile']['size'];
    $fileTmpName  = $_FILES['myfile']['tmp_name'];
    $fileType = $_FILES['myfile']['type'];
    $fileExtension = @strtolower(end(explode('.',$fileName)));

    if (isset($_POST['submit'])) {

		

        if (! in_array($fileExtension,$fileExtensions)) {
            $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
        }

        if ($fileSize > 5000000000) {
            $errors[] = "This file is more than 5GB. Sorry, it has to be less than or equal to 5000MB";
        }

        if (empty($errors)) {
			$response = $publitio_api->upload_file($fileTmpName, "file"); 

               print_r($response);

        } else {
            foreach ($errors as $error) {
                echo $error . "These are the errors" . "\n";
            }
        }
    }


?>